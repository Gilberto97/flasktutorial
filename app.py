from flask import Flask, render_template, url_for, flash, redirect
from forms import AddProductForm
app = Flask(__name__)

app.config['SECRET_KEY'] = 'Gus9HGQrzf6HcAJn'

products = [
    {
        'name':'Apple',
        'price':2.50,
        'unit':'kg',
        'category':'Fruit'
    },
    {
        'name':'Orange',
        'price':4.00,
        'unit':'kg',
        'category':'Fruit'
    },
    {
        'name':'Banana',
        'price':4.50,
        'unit':'kg',
        'category':'Fruit'
    }
]

@app.route('/')
def index():
    return render_template('index.html', products=products)

@app.route('/about')
def about():
    return '<h1>About Page</h1>'

@app.route('/addproduct', methods=['POST','GET'])
def add_product():
    form = AddProductForm()

    if form.validate_on_submit():
        print(form.data)

        products.append({
            'name': form.name.data,
            'price': form.price.data,
            'unit': form.unit.data,
            'category': form.category.data,
        })
        return redirect(url_for('index'))
    return render_template('add_product.html', form=form)

if __name__ == "__main__":
    app.run(debug=True)